mkdir
code

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

#git remote add origin https://gitlab.com/RasmusLievonen/unit-test
#git remote set-url origin
# git push -u origin master

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

#test files that go to staging area
 git add . --dry-run